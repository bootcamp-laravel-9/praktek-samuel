@extends('layout/main')
@section('menu-tiket', 'active')
@section('menu-title', 'Tambah Tiket')
@section('content')
  <div class="row match-height">
    <div class="col-md-12 col-12">
      <div class="card">
        <div class="card-content">
          <div class="card-body">
            <form class="form form-vertical" action="{{ url('/ticket/create') }}" method="POST">
              @csrf
              <div class="form-body">
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label for="name">Nama</label>
                      <input type="text" id="name" class="form-control" name="name" placeholder="Nama"
                        value="{{ old('name') }}">
                      @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" id="email" class="form-control" name="email" placeholder="Email"
                        value="{{ old('email') }}">
                      @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="no_telp">No Telp</label>
                      <input type="text" id="no_telp" class="form-control" name="no_telp" placeholder="No Telp"
                        value="{{ old('no_telp') }}">
                      @error('no_telp')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group mb-3">
                      <label for="address" class="form-label">Alamat</label>
                      <textarea class="form-control" id="address" name="address" rows="3">{{ old('address') }}</textarea>
                      @error('address')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="date_ticket">Tanggal</label>
                      <input type="date_ticket" class="form-control mb-3 flatpickr-no-config"
                        placeholder="{{ date('Y-m-d') }}" name="date_ticket" id="date_ticket">
                      @error('date_ticket')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="payment_method">Metode Pembayaran</label>
                      <fieldset class="form-group">
                        <select class="form-select" id="payment_method" name="payment_method">
                          <option>QRIS</option>
                          <option>GOPAY</option>
                          <option>Transfer Bank</option>
                        </select>
                      </fieldset>
                      @error('payment_method')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="total_ticket">Total</label>
                      <input type="number" id="total_ticket" class="form-control" name="total_ticket" min="1"
                        placeholder="0" value="{{ old('total_ticket') }}">
                      @error('total_ticket')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="category">Kategori</label>
                      <fieldset class="form-group">
                        <select class="form-select" id="tiket_category" name="tiket_category">
                          @foreach ($ticketCategory as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                          @endforeach
                        </select>
                      </fieldset>
                      @error('tiket_category')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="col-12 d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary me-1 mb-1">Simpan</button>
                    <a href="/" class="btn btn-light-secondary me-1 mb-1">Batal</a>
                  </div>
                </div>
              </div>
            </form>
            @if (Session::has('message'))
              <script>
                Swal.fire({
                  title: '{{ Session::get('message') }}',
                  confirmButtonText: "Ok",
                  icon: "success"
                }).then((result) => {
                  if (result.isConfirmed) {
                    // return window.location.href = "{{ url('/ticket/create') }}";
                  }
                });
              </script>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
