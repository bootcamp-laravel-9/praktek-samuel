@extends('layout/main')
@section('menu-tiket', 'active')
@section('menu-title', 'Daftar Tiket')
@section('content')
  <div class="card">
    <div class="card-header">
      <h5 class="card-title">
        Simple Datatable
      </h5>
    </div>
    <div class="card-body">
      <div class="mb-3">
        <label for="searchByCategory" class="form-label">Search by Category:</label>
        <select id="searchByCategory" class="form-select">
          <option value=""">Semua kategori</option>
          @foreach ($ticketCategory as $item)
            <option value="{{ $item->id }}">{{ $item->name }}</option>
          @endforeach
        </select>
      </div>
      <table class="table table-striped" id="table1">
        <thead>
          <tr>
            <th>NO</th>
            <th>NOMOR TIKET</th>
            <th>NAMA</th>
            <th>Email</th>
            <th>NO TELEPON</th>
            <th>KATEGORI</th>
            <th>TANGGAL TIKET</th>
            <th>TOTAL</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($data as $item)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $item->no_tiket }}</td>
              <td>{{ $item->nama }}</td>
              <td>{{ $item->email }}</td>
              <td>{{ $item->no_telp }}</td>
              <td>{{ $item->category }}</td>
              <td>{{ $item->date_ticket }}</td>
              <td>{{ $item->total_ticket }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection
