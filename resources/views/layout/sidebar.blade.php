<div id="sidebar">
  <div class="sidebar-wrapper active">
    <div class="sidebar-header position-relative">
      <div class="d-flex justify-content-between align-items-center">
        <div class="logo">
          <a href="/"><img src="{{ asset('icons/logoHero.svg') }}" alt="Logo" srcset=""
              style="height: 100%"></a>
        </div>
        <div class="sidebar-toggler  x">
          <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
        </div>
      </div>
    </div>
    <div class="sidebar-menu">
      <ul class="menu">
        <li class="sidebar-title">Menu</li>

        <li class="sidebar-item @yield('menu-admin') ">
          <a href="/" class='sidebar-link'>
            <i class="bi bi-grid-fill"></i>
            <span>Admin</span>
          </a>
        </li>
        <li class="sidebar-item @yield('menu-tiket') has-sub ">
          <a href="index.html" class='sidebar-link'>
            <i class="bi bi-grid-fill"></i>
            <span>Tiket</span>
          </a>
          <ul class="submenu ">
            <li class="submenu-item  ">
              <a href="{{ url('/ticket/create') }}" class="submenu-link"><i class="fa-solid fa-ticket"></i> <span>Beli
                  Tiket</span></a>
            </li>
            <li class="submenu-item  ">
              <a href="{{ url('/ticket') }}" class="submenu-link"><i class="fa-solid fa-list"></i>
                <span>Report Tiket</span></a>
            </li>
          </ul>
        </li>
        <li class="sidebar-item">
          <a href="{{ url('/logout') }}" class="btn btn-outline-danger btn-sm" style="width: 100%" id="logout"><i
              class="fa-solid fa-right-from-bracket"></i><span>Logout</span></a>
        </li>
      </ul>

    </div>
  </div>
</div>
