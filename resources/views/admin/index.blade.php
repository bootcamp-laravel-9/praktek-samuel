@extends('layout/main')
@section('menu-admin', 'active')
@section('menu-title', 'Master Admin')
@section('content')
  <div class="row" id="table-striped" style="flex-direction: column; align-items: end">
    <div class="btn-add" style="width: 12%; padding-right: 12px; padding-bottom: 12px">
      <a href="{{ url('/create') }}" class="btn btn-outline-primary block">Tambah</a>
    </div>
    <div class="col-12 col-md-12">
      <div class="card">
        <div class="card-content">
          <div class="card-body" style="padding: 8px">
            <!-- Table with outer spacing -->
            <div class="table-responsive">
              <table table class="table table-striped" id="table1">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>NAME</th>
                    <th>EMAIL</th>
                    <th>ACTIONS</th>
                  </tr>
                </thead>
                <tbody>
                  {{-- @dd($data) --}}
                  @foreach ($data as $item)
                    <tr>
                      <td class="text-bold-500">{{ $loop->iteration }}</td>
                      <td class="text-bold-500">{{ $item->name }}</td>
                      <td class="text-bold-500">{{ $item->email }}</td>
                      <td>
                        <a href="{{ url('/edit/' . $item->id) }}" class="btn btn-outline-primary btn-sm"><i
                            class="fa-solid fa-pen-to-square"></i></a>
                        <a href="{{ url('/delete/' . $item->id) }}" class="btn btn-outline-danger btn-sm"
                          id="deleteUserBtn"><i class="fa-solid fa-trash"></i></a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
