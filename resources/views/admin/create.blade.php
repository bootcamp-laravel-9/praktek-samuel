@extends('layout/main')
@section('menu-admin', 'active')
@section('menu-title', $isEdit? 'Edit Admin' : 'Tambah Admin')
@section('content')
  <div class="row match-height">
    <div class="col-md-12 col-12">
      <div class="card">
        <div class="card-content">
          <div class="card-body">
            @if (!$isEdit)
              <form class="form form-vertical" action="{{ url('/create-process') }}" method="POST">
                @csrf
                <div class="form-body">
                  <div class="row">
                    <div class="col-12">
                      <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" class="form-control" name="name" placeholder="Name"
                          value="{{ old('name') }}">
                        @error('name')
                          <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" id="email" class="form-control" name="email" placeholder="Email"
                          value="{{ old('email') }}">
                        @error('email')
                          <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" id="password" class="form-control" name="password" placeholder="Password">
                        @error('password')
                          <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                      </div>
                    </div>
                    <div class="col-12 d-flex justify-content-end">
                      <button type="submit" class="btn btn-primary me-1 mb-1">Simpan</button>
                      <a href="/" class="btn btn-light-secondary me-1 mb-1">Batal</a>
                    </div>
                  </div>
                </div>
              </form>
            @else
              <form class="form form-vertical" action="{{ url('/edit-process') }}" method="POST">
                @csrf
                <div class="form-body">
                  <div class="row">
                    <div class="col-12">
                      <div class="form-group">
                        <label for="name">Name</label>
                        <input type="hidden" id="id" class="form-control" name="id"
                          value="{{ $user->id }}">
                        <input type="text" id="name" class="form-control" name="name" placeholder="Name"
                          value="{{ $user->name }}">
                        @error('name')
                          <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" id="email" class="form-control" name="email" placeholder="Email"
                          value="{{ $user->email }}">
                        @error('email')
                          <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" id="password" class="form-control" name="password" placeholder="Password">
                        @error('password')
                          <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                      </div>
                    </div>
                    <div class="col-12 d-flex justify-content-end">
                      <button type="submit" class="btn btn-primary me-1 mb-1">Simpan</button>
                      <a href="/" class="btn btn-light-secondary me-1 mb-1">Batal</a>
                    </div>
                  </div>
                </div>
              </form>
            @endif

            @if (Session::has('message'))
              <script>
                Swal.fire({
                  title: '{{ Session::get('message') }}',
                  confirmButtonText: "Ok",
                  icon: "success"
                }).then((result) => {
                  if (result.isConfirmed) {
                    return window.location.href = "{{ url('/') }}";
                  }
                });
              </script>
            @endif

          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
