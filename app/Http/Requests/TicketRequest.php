<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            "no_tiket"  => ['string'],
            'name' => ['required', 'regex:/^[^\W\d_]+$/'],
            'email' => ['required', 'string', 'email'],
            "no_telp"  => ['integer'],
            "address"  => ['required', 'string'],
            "date_ticket"  => ['required', 'date'],
            "total_ticket"  => ['required', 'min:1'],
        ];
    }
}
