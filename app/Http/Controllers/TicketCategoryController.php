<?php

namespace App\Http\Controllers;

use App\Models\TicketCategory;
use App\Repositories\TicketCategoryRepository;
use Illuminate\Http\Request;

class TicketCategoryController extends Controller
{
    protected $ticketRepository;

    public function __construct(TicketCategoryRepository $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;
    }

    public function index()
    {
        $data = TicketCategory::all();
        return $data;
    }
}
