<?php

namespace App\Http\Controllers;

use App\Models\TicketCategory;
use Illuminate\Http\Request;

class ConsumeTicketApiController extends Controller
{

    public function index()
    {
        $request = Request::create(url('api/ticket'), 'GET');
        $request->headers->set('Accept', 'application/json');
        $request->headers->set('X-API-KEY', 'Bootcamp_9');
        $response = app()->make('router')->dispatch($request);
        // dd(json_decode($response->getContent()));
        $data = json_decode($response->getContent());

        $ticketCategory = TicketCategory::all();
        return view('ticket.report', ['data' => $data, 'ticketCategory' => $ticketCategory]);
    }

    public function create()
    {
        $dataCategory = TicketCategory::all();
        // $request = Request::create(url('api/ticket/create'), 'POST');
        // $request->headers->set('Accept', 'application/json');
        // $request->headers->set('X-API-KEY', 'Bootcamp_9');
        // $response = app()->make('router')->dispatch($request);
        return view('ticket.create', ['ticketCategory' => $dataCategory]);
    }

    public function store(Request $request)
    {
        $request = Request::create(url('api/ticket/create'), 'POST');
        $request->headers->set('Accept', 'application/json');
        $request->headers->set('X-API-KEY', 'Bootcamp_9');
        $response = app()->make('router')->dispatch($request);
        // dd($response);

        if ($response->exception == null) {
            return back()->with('message', $response->original['message']);
        } else {
            return back()->withInput()->withErrors($response->original['errors']);
        }
    }
}
