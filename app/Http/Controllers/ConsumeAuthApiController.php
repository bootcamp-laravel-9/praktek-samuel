<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ConsumeAuthApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $request = Request::create(url('api/login'), 'POST');
        // $request->headers->set('Accept', 'application/json');
        // $request->headers->set('X-API-KEY', 'Bootcamp_9');

        $apiRequest = Request::create(url('api/login'), 'POST', [
            'email' => $request->email,
            'password' => $request->password,
        ]);

        $apiRequest->headers->set('Accept', 'application/json');
        $apiRequest->headers->set('X-API-KEY', 'Bootcamp_9');
        $response = app()->handle($apiRequest);
        // dd($response);
        if ($response->getStatusCode() == 200) {
            $token = $response->original['token'];
            $request->session()->put('LoginSession', $token);
            return redirect('/');
        } else {
            return back()->withInput()->with('error', 'Email or Password is incorrect');
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
