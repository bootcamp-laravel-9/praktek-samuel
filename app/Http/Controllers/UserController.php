<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function index(Request $request)
    {
        $data = User::all();
        return view('admin/index', ['data' => $data]);
    }

    public function create()
    {
        return view('admin/create', ['isEdit' => false]);
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => ['required', 'regex:/^[^\W\d_]+$/'],
            'email' => ['required', 'string', 'email', 'unique:users,email'],
            'password' => ['required', 'min:8']
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
        return back()->with('message', 'User created successfully');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('admin/create', ['isEdit' => true, 'user' => $user]);
    }

    public function update(Request $request)
    {
        $updateUser = User::find($request->id);
        $validated = $request->validate([
            'name' => ['required', 'regex:/^[^\W\d_]+$/'],
            'email' => ['required', 'string', 'email'],
            'password' => ['nullable', 'min:8']
        ]);

        $updateUser->name = $request->name;
        $updateUser->email = $request->email;

        $request->password ? $updateUser->password = Hash::make($request->password) : '';

        if ($updateUser->save()) {
            return back()->with('message', 'User updated successfully');
        } else {
            return redirect()->back()->withInput();
        }
    }

    public function delete($id)
    {
        $user = User::destroy($id);
        return redirect('/');
    }
}
