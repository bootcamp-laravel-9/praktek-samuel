<?php

namespace App\Repositories;

use App\Http\Resources\TicketCategoryResource;
use App\Models\TicketCategory;

class TicketCategoryRepository
{
    protected $ticketCategory;

    public function __construct(TicketCategory $ticketCategory)
    {
        $this->ticketCategory = $ticketCategory;
    }

    public function all()
    {
        $response = TicketCategoryResource::collection($this->ticketCategory->all());
        return $response;
    }
}
