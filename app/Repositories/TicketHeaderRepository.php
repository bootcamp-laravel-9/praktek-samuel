<?php

namespace App\Repositories;

use App\Http\Resources\TicketHeaderResource;
use App\Models\TicketHeader;

class TicketHeaderRepository
{
    protected $ticketHeader;

    public function __construct(TicketHeader $ticketHeader)
    {
        $this->ticketHeader = $ticketHeader;
    }

    public function store($request)
    {
        $data = $this->ticketHeader->create(
            [
                'no_ticket' => $request->no_ticket,
                'nama' => $request->nama,
                'email' => $request->email,
                'no_telp' => $request->no_telp,
                'address' => $request->address,
                'date_ticket' => $request->date_ticket
            ]
        );
        dd($data);
    }

    public function all()
    {
        $response = TicketHeaderResource::collection($this->ticketHeader->all());
        return $response;
    }
}
