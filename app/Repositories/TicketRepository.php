<?php

namespace App\Repositories;

use App\Models\TicketCategory;
use App\Models\TicketDetail;
use App\Models\TicketHeader;
use Illuminate\Support\Facades\DB;

class TicketRepository
{
    public function getTicketCategory()
    {
        $data = TicketCategory::all();
        return $data;
    }

    public function getTicket()
    {
        $data =  DB::table('ticket_headers')
            ->join('ticket_details', 'ticket_headers.id', '=', 'ticket_details.ticket_header_id')
            ->join('ticket_categories', 'ticket_details.tiket_category', '=', 'ticket_categories.id')
            ->select('ticket_headers.*', 'ticket_details.*', 'ticket_categories.name as category')
            ->get();
        return $data;
    }

    public function store($request)
    {
        $ticketHeader = TicketHeader::create([
            'no_tiket' => 'tiket-' . Date('dmy'),
            'nama' => $request->name,
            'email' => $request->email,
            'no_telp' => $request->no_telp,
            'address' => $request->address,
            'date_ticket' => $request->date_ticket
        ]);

        TicketDetail::create([
            'ticket_header_id' => $ticketHeader->id,
            'tiket_category' => $request->tiket_category,
            'total_ticket' => $request->total_ticket
        ]);
        // dd($request);

        // dd($ticketHeader);
        return $ticketHeader;
    }
}
