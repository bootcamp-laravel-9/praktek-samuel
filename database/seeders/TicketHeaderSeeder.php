<?php

namespace Database\Seeders;

use App\Models\TicketHeader;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TicketHeaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TicketHeader::create(
            [
                'no_tiket' => 'tiket_12345',
                'nama' => 'Admin',
                'email' => 'admin@example.com',
                'no_telp' => '1234567',
                'address' => 'admin address',
                'date_ticket' => now()

            ]
        );
    }
}
