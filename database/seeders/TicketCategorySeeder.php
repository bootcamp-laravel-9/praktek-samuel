<?php

namespace Database\Seeders;

use App\Models\TicketCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TicketCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TicketCategory::create(
            [
                'name' => 'Dewasa',
                'detail' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Obcaecati, eos.'
            ]
        );
        TicketCategory::create(
            [
                'name' => 'Anak-anak',
                'detail' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Obcaecati, eos.'
            ]
        );
        TicketCategory::create(
            [
                'name' => 'Remaja',
                'detail' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Obcaecati, eos.'
            ]
        );
    }
}
