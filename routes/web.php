<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('admin/index');
// });

// Route::get('/login', 'ConsumeAuthApiController@index')->name('login');
// Route::post('/login', 'ConsumeAuthApiController@create');


Route::group(['middleware' => ['isNotLogin']], function () use ($router) {
    Route::get('/login', 'ConsumeAuthApiController@index')->name('login');
    Route::post('/login', 'ConsumeAuthApiController@create');
});

Route::group(['middleware' => ['isLogin']], function () use ($router) {
    Route::get('/', 'UserController@index');
    Route::get('/create', 'UserController@create');
    Route::post('/create-process', 'UserController@store');
    Route::get('/edit/{id}', 'UserController@edit');
    Route::post('/edit-process', 'UserController@update');
    Route::get('/delete/{id}', 'UserController@delete');

    Route::get('/ticket', 'ConsumeTicketApiController@index');
    Route::get('/ticket/create', 'ConsumeTicketApiController@create');
    Route::post('/ticket/create', 'ConsumeTicketApiController@store');
    Route::get('/logout', 'ConsumeAuthApiController@logout');
});
