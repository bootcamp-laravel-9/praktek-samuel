<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('users', 'UserController@index');
// Route::post('create-user', 'UserController@store');
// Route::put('update-user', 'UserController@update');
// Route::delete('delete-user/{id}', 'UserController@delete');

Route::post('login', 'AuthController@login');

Route::group(['middleware' => ['auth:sanctum', 'api.key']], function () use ($router) {
    Route::get('ticket-category', 'TicketCategoryController@index');
    Route::get('ticket-header', 'TicketHeaderController@index');
    Route::get('ticket', 'TicketController@index');
    Route::post('ticket/create', 'TicketController@store');
});
